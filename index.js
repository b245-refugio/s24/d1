console.log("ES6 UPDATES");

// [SECTION] Exponent Operator
// before ES6
	const firstNum = 8 ** 2;
	console.log(firstNum);

	// ES6
	/*
		Syntax: Math.pow(base,exponent);

	*/
	const secondNum = Math.pow(8,2);
	console.log(secondNum);

	// [SECTION] Template Literals
		// allows us to write strings without using concatenation operator(+);
	// greatly helps with code readability

	let name = "John";
	// Before ES6
		let message = "Hello " + name + " ! Welcome to programming."
		console.log(message);

	// after ES6
		// uses backticks(``)

		message = `Hello ${name} ! Welcome to programming.`
		console.log(message);

		// Template literals allows us to perform operations

		const interestRate = 0.1;
		const principal = 1000;

		console.log(`The interest on your savings account is: ${interestRate*principal}.`);

		// [SECTION] Array destructuring
			// it allows us to unpack elements in an array into distinct variables
			// allows us to name array elements with variables instead using index number
		/*
			Syntax:
				let/const [variableNameA, variableNameB,...] = arrrayName;

		*/

		const fullName = ["Juan", "Dela", "Cruz"];

		// brefore ES6

		let firstName = fullName[0];
		let middleName = fullName[1];
		let lastName = fullName[2];

		console.log(`Hello ${firstName} ${middleName} ${lastName}.`);

		// after ES6 updates

		const[ mName,fName,lName] = fullName;

		console.log(fName);
		console.log(mName);
		console.log(lName);

		// Mini-activity
			// Array destructuring
		let array = [1, 2, 3, 4, 5];

			//destructure the array into 5 different variables

		const [num1,num2,num3,num4,num5] = array;

		console.log(num1);
		console.log(num2);
		console.log(num3);
		console.log(num4);
		console.log(num5);

		// [SECTION] Object Destructioning
			// allows us to unpack properties of objects into distinct variables
			/*
				Syntax:
					let/const {propertyNameA,propertyNameB,....} = objectName;


			*/

		const person = {
			givenName: "Jane",
			maidenName: "Dela",
			familyName: "Cruz"
		}

		// before ES6

		let gName = person.givenName;
		let midName = person.maidenName;
		let famName = person.familyName;

		console.log(gName);
		console.log(midName);
		console.log(famName);

		// after ES6 update

		let {givenName,maidenName,familyName} = person;

		console.log("Object destructuring after ES6");
		console.log(givenName);
		console.log(maidenName);
		console.log(familyName);

		// [SECTION] Arrow Function
			// compact alternative syntax to traditional functions

		const hello = () =>{
			console.log("Hello World!");
		}

		hello();

		// // function expression
		// 	const hello = function(){
		// 		console.log("Hello World!");
		// 	}

		// // function declaration
		// 	function hello(){
		// 		console.log("Hello World!");
		// 	}

// [SECTION] Implicit Return
	/*
		there are intances when you can omit return statement this works because even without using return keyword

	*/
		// single line para auto return
		const add = (x,y) => x+y;

		console.log("Implicit return:");
		console.log(add(1,2));
		// if may curly braces need return keyword
		const subtract = (x,y) => {
			return x-y
		};
		console.log(subtract(10,5));

	// [SECTION] Default Function Argument Value

		/*const greet = (firstName = "firstName", lastName = "lastName") =>{
				return `Good afternoon, ${firstName} ${lastName} !`;
		}

		console.log(greet());*/

		function greet(firstName = "firstName", lastName = "lastName"){
			return `Good afternoon, ${firstName} ${lastName} !`;
		}

		console.log(greet());

	// [SECTION] Class-based Object Blueprints
		// allows us to create/instantiation of object using clasees blueprints
		// create class
			// constructor is a special method of a class for creating/initializing an object of the class
		/*
			Syntax:
				class className{
				constructor(objectValueA,objectValueB,...){
					this.objectPropertyA = objectValueA;
					this.objectPropertyB = objectValueB
				}
				}


		*/

		class Car{
			constructor(brand, name, year){
				this.carBrand = brand;
				this.carName = name;
				this.carYear = year;
			}
		}

		let car = new Car("Toyota", "Hilux-pickup",2015)
		console.log(car);

		car.carBrand = "Nissan"
		console.log(car);